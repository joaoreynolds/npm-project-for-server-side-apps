Npm | Webpack | Sass Starter for server-side websites (not SPAs)
------------

Install node dependencies with `npm install`

Run `npm run dev` while developing. This will:

 - Process and compile sass (scss) files as you're working
 - Run JS through a linter
 - Compile JS using webpack
 - Serves JS files from memory using a node server (The webpacked scripts is served at `http://localhost:8080/js/scripts.js`)

### Building ###

Run `npm run build` to package up the theme into the `build` directory. This will:

 - Copy every to the `build` directory.
 - Minify/uglify javascript
 - Minify CSS (todo)
 - Reversion JS and CSS assets


### Todo ###

 - Minify CSS
 - Remove localhost reference from html
