module.exports = {
    "extends": "standard",
    "plugins": [
        "standard"
    ],
    "rules": {
      semi: [2, "always"],
      "space-before-function-paren": [0],
      "no-new": [0],
    }
};
