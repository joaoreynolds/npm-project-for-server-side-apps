module.exports = {
  entry: {
    main: [
      'webpack-dev-server/client?http://localhost:8080',
      './dev/js/app/app',
    ]
  },
  output: {
    filename: 'scripts.js',
    path: './.tmp/js/',
    publicPath: '/js/'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|build)/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.js$/,
        loader: "eslint-loader",
        exclude: /(node_modules|build)/
      },
    ],
    eslint: {
      configFile: '.eslintrc.js'
    }
  },
};
